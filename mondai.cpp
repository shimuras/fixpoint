#include "mondai.h"

using namespace std;

void mondai::result(int N, int m, int t) {
	string filename = "C:../02.txt";
	ifstream stream(filename);
	if (!stream) {
		std::cout << "Can't read " << filename << std::endl;
		exit(0);
	}
	string line;
	vector<vector<string>> data(3);
	int n = 0;
	while (getline(stream, line)) {
		string str = "";
		for (int i = 0; i < line.size(); ++i) {
			str += line[i];
			if (line[i] == ',' || i == line.size() - 1) {
				data.at(n).push_back(str);
				++n;
				str = "";
				if (n == 3) break;
			}
		}
		n = 0;
	}
	double sum = 0;
	for (int j = 0; j < data[2].size(); ++j) {
		if (j + m < data[2].size()) {
			for (int l = 0; l < m; ++l) {
				if (data[2][j + l] == "-") {
					sum += 0;
				}
				if (data[2][j + l] != "-") {
					sum += stoi(data[2][j + l]);
				}
				if (l == m - 1 && sum / m > t) {
					var.start = j;
					var.finish = j + m - 1;
					std::cout << data[0][var.start] << ":" << data[1][var.start] << "" << data[0][var.finish] << ":" << data[1][var.finish] << "設問3" << endl;
					var.year_A = stoi((data[0][var.start]).substr(0, 4));
					var.mon_A = stoi((data[0][var.start]).substr(4, 2));
					var.mday_A = stoi((data[0][var.start]).substr(6, 2));
					var.hour_A = stoi((data[0][var.start]).substr(8, 2));
					var.min_A = stoi((data[0][var.start]).substr(10, 2));
					var.sec_A = stoi((data[0][var.start]).substr(12, 2));
					var.year_B = stoi((data[0][var.finish]).substr(0, 4));
					var.mon_B = stoi((data[0][var.finish]).substr(4, 2));
					var.mday_B = stoi((data[0][var.finish]).substr(6, 2));
					var.hour_B = stoi((data[0][var.finish]).substr(8, 2));
					var.min_B = stoi((data[0][var.finish]).substr(10, 2));
					var.sec_B = stoi((data[0][var.finish]).substr(12, 2));
					time_diff();
				}
				sum = 0;
			}
		}
		auto string = std::string(data[1][j]);
		auto separator = std::string(".");
		auto separator_length = separator.length();
		auto list = std::vector<std::string>();
		if (data[2][j] == "-") {
			if (separator_length == 0) {
				list.push_back(string);
			}
			else {
				auto offset = std::string::size_type(0);
				while (1) {
					auto pos = string.find(separator, offset);
					if (pos == std::string::npos) {
						list.push_back(string.substr(offset));
						break;
					}
					list.push_back(string.substr(offset, pos - offset));
					offset = pos + separator_length;
				}
			}
			for (int q = 0; q < data[2].size(); ++q) {
				auto string = std::string(data[1][q]);
				auto list_d = std::vector<std::string>();
				if (separator_length == 0) {
					list_d.push_back(string);
				}
				else {
					auto offset = std::string::size_type(0);
					while (1) {
						auto pos = string.find(separator, offset);
						if (pos == std::string::npos) {
							list_d.push_back(string.substr(offset));
							break;
						}
						list_d.push_back(string.substr(offset, pos - offset));
						offset = pos + separator_length;
					}
				}
				if (list[0] == list_d[0] && list[1] == list_d[1] && list[2] == list_d[2]) {
					++var.check_1;
				}
				if (list[0] == list_d[0] && list[1] == list_d[1] && list[2] == list_d[2] && data[2][q] == "-") {
					++var.check_2;
				}
			}
			//cout << var.check_1 << "" << var.check_2 << endl;
			if (var.check_1 == var.check_2) {
				cout << data[0][j] << ":" << data[1][j] << "設問4" << endl;
			}
			var.check_1 = 0;
			var.check_2 = 0;
		}
		if (N == 1) {
			if (data[2][j] == "-" && var.start == 0) {
				var.start = j;
			}
			if (var.start != 0 && data[2][j] != "-") {
				var.finish = j;
				std::cout << data[0][var.start] << ":" << data[1][var.start] << "" << data[0][var.finish] << ":" << data[1][var.finish] << "設問1" << endl;
				var.year_A = stoi((data[0][var.start]).substr(0, 4));
				var.mon_A = stoi((data[0][var.start]).substr(4, 2));
				var.mday_A = stoi((data[0][var.start]).substr(6, 2));
				var.hour_A = stoi((data[0][var.start]).substr(8, 2));
				var.min_A = stoi((data[0][var.start]).substr(10, 2));
				var.sec_A = stoi((data[0][var.start]).substr(12, 2));
				var.year_B = stoi((data[0][var.finish]).substr(0, 4));
				var.mon_B = stoi((data[0][var.finish]).substr(4, 2));
				var.mday_B = stoi((data[0][var.finish]).substr(6, 2));
				var.hour_B = stoi((data[0][var.finish]).substr(8, 2));
				var.min_B = stoi((data[0][var.finish]).substr(10, 2));
				var.sec_B = stoi((data[0][var.finish]).substr(12, 2));
				time_diff();
			}
		}
		if (N > 1) {
			for (int k = 0; k < N; ++k) {if (data[2][j + k] != "-" )break;
				if (k == N - 1) {
					var.start = j;
					var.finish = j + N;
					std::cout << data[0][var.start] << ":" << data[1][var.start] << "" << data[0][var.finish] << ":" << data[1][var.finish] << "設問2" << endl;
					var.year_A = stoi((data[0][var.start]).substr(0, 4));
					var.mon_A = stoi((data[0][var.start]).substr(4, 2));
					var.mday_A = stoi((data[0][var.start]).substr(6, 2));
					var.hour_A = stoi((data[0][var.start]).substr(8, 2));
					var.min_A = stoi((data[0][var.start]).substr(10, 2));
					var.sec_A = stoi((data[0][var.start]).substr(12, 2));
					var.year_B = stoi((data[0][var.finish]).substr(0, 4));
					var.mon_B = stoi((data[0][var.finish]).substr(4, 2));
					var.mday_B = stoi((data[0][var.finish]).substr(6, 2));
					var.hour_B = stoi((data[0][var.finish]).substr(8, 2));
					var.min_B = stoi((data[0][var.finish]).substr(10, 2));
					var.sec_B = stoi((data[0][var.finish]).substr(12, 2));
					time_diff();
					//cout << var.start << endl << var.finish << endl;
				}
			}
		}
	}
}
void mondai::time_diff() {
	struct tm tmTimeA = { 0 };
	tmTimeA.tm_year = var.year_A - 1900;
	tmTimeA.tm_mon = var.mon_A - 1;
	tmTimeA.tm_mday = var.mday_A;
	tmTimeA.tm_hour = var.hour_A;
	tmTimeA.tm_min = var.min_A;
	tmTimeA.tm_sec = var.sec_A;

	struct tm tmTimeB = { 0 };
	tmTimeB.tm_year = var.year_B - 1900;
	tmTimeB.tm_mon = var.mon_B - 1;
	tmTimeB.tm_mday = var.mday_B;
	tmTimeB.tm_hour = var.hour_B;
	tmTimeB.tm_min = var.min_B;
	tmTimeB.tm_sec = var.sec_B;
	double diff = difftime(
		mktime(&tmTimeB),
		mktime(&tmTimeA));
	std::printf("%f", diff);
	std::cout << endl;
	var = { 0 };
	tmTimeA = { 0 };
	tmTimeB = { 0 };
}