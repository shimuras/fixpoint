#pragma once
#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <time.h>

struct Variable {
	int start = 0;
	int finish = 0;
	int year_A = 0;
	int mon_A = 0;
	int mday_A = 0;
	int hour_A = 0;
	int min_A = 0;
	int sec_A = 0;
	int year_B = 0;
	int mon_B = 0;
	int mday_B = 0;
	int hour_B = 0;
	int min_B = 0;
	int sec_B = 0;
	int check_1 = 0;
	int check_2 = 0;
};

class mondai {
public:
	Variable var;
	void result(int N, int m, int t);
private:
	void time_diff();
};

class mondai2 {
public:
	void result();
};